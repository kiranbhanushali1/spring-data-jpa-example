package com.example.springdatajpaexample.repository;

import com.example.springdatajpaexample.entity.Course;
import com.example.springdatajpaexample.entity.Teacher;
import org.hibernate.usertype.CompositeUserType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TeacherRepositoryTest {

    @Autowired
    TeacherRepository teacherRepository;

    @Test
    public void saveTeacher(){
        List<Course> courseList = List.of(
                Course.builder().title("java").credit(10).build(),
                Course.builder().title("python").credit(20).build()
        );
        Teacher teacher = Teacher.builder()
                .firstName("abc").lastName("xyz")
                .courses(courseList)
                .build();
        teacherRepository.save(teacher);
    }

}