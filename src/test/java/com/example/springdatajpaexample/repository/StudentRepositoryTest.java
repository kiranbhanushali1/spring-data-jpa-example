package com.example.springdatajpaexample.repository;

import com.example.springdatajpaexample.entity.Guardian;
import com.example.springdatajpaexample.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;



@SpringBootTest

class StudentRepositoryTest {
    @Autowired
    private StudentRepository studentRepository;

    @Test
    public void saveStudent() {
        Student student = Student.builder()
                .studentId(1L)
                .emailId("kiran@gamil.com")
                .firstName("kiran")
                .lastName("bhanushali")
//                .guardianEmail("nitin@gmail.com")
//                .guardianName("nitin")
//                .guardianMobile("12344")
                .build();
        studentRepository.save(student);
    }

    @Test
    public void saveStudentWithGuardian() {
        Guardian guardian = Guardian.builder()
                .guardianEmail("nitin1@gmail.com")
                .guardianName("nitin")
                .guardianMobile("12344")
                .build();
        Student student = Student.builder()
                .studentId(2L)
                .emailId("bhraat@gamil.com")
                .firstName("bharat")
                .lastName("bhanushali")
                .guardian(guardian)
                .build();
        studentRepository.save(student);
    }

    @Test
    public void getAllStudents() {
        List<Student> students = studentRepository.findAll();
        System.out.println("Student List: " + students);
    }


    @Test
    public void findStudentByFirstName() {
        System.out.println(studentRepository.findByFirstName("kiran"));
    }

    @Test
    public void findStudentByFirstNameContaining() {
        System.out.println(studentRepository.findByFirstNameContaining("ki"));
    }

    @Test
    public void findStudentByFirstNameAndLastName() {
        System.out.println(studentRepository.findByFirstNameAndLastName("kiran", "bhanushali"));
    }

    @Test
    public  void findStudentByEmailAddress(){
        System.out.println(studentRepository.getStudentByEmailAddress("kiran@gmail.com"));
    }

    @Test
    public void updateStudentFirstNameByEmailAddress(){
        System.out.println(studentRepository.updateStudentNameByEmailId("bharat new name","bhraat@gamil.com"));
        getAllStudents();;
    }

}