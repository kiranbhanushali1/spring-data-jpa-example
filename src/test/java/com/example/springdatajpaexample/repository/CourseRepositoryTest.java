package com.example.springdatajpaexample.repository;

import com.example.springdatajpaexample.entity.Course;
import org.apache.catalina.LifecycleState;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CourseRepositoryTest {
    @Autowired
    CourseRepository courseRepository;

    @Test
    public void printAllCources(){
        // gives only the courses not course material
        // for getting course material also we have to bidirecrional mapping
        // between course and courseMaterial
        List<Course> courseList = courseRepository.findAll();
        System.out.println(courseList);

    }

    @Test
    public void printCoursesPagination(){
        Pageable firstTwoRecords = PageRequest.of(0,2);
        Pageable nextTwoRecords = PageRequest.of( 3, 2 ) ;

        List<Course> courses = courseRepository.findAll(firstTwoRecords).getContent();
        System.out.println("First page:"+ courses);

        List<Course> courses1 = courseRepository.findAll(nextTwoRecords).getContent();
        System.out.println("Second page:" +courses);
    }

}