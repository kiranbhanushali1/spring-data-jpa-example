package com.example.springdatajpaexample.repository;

import com.example.springdatajpaexample.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {

    // only have to define methods
    public List<Student> findByFirstName(String firstName);

    public List<Student> findByFirstNameContaining(String name);


    public Student findByFirstNameAndLastName(String firstName ,String lastName);


    // with curstom JP query ( simlar syntax to sql )
    @Query("select s from  Student s where s.emailId=?1")
    Student getStudentByEmailAddress(String emailId);


    // native sql query
//    @Query(
//            value = "select first_name from student",
//            nativeQuery = true
//    )

    // for upadate @Transactional annotation

//    @Modifying
//    @Transactional

    @Modifying
    @Transactional
    @Query(
            value = "update student set first_name = ?1 where email_id=?2",
            nativeQuery = true
    )
    int updateStudentNameByEmailId(String firstName,String emailId);


}
