package com.example.springdatajpaexample.repository;

import com.example.springdatajpaexample.entity.CourseMaterial;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseMaterialRepository extends JpaRepository<CourseMaterial,Long> {
}
