package com.example.springdatajpaexample.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name="student",
    uniqueConstraints = @UniqueConstraint(
        name = "emailId",
        columnNames = "emailId"
    )
)
public class Student {
    @Id
    private Long studentId;
    private String firstName;
    private String lastName;
    private String emailId;

    @Embedded
    private Guardian guardian;
}
