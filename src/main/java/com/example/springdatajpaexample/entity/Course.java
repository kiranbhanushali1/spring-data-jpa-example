package com.example.springdatajpaexample.entity;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.Objects;

@Data
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Course{
    @Id
    @GeneratedValue
    private Long courseId;
    private String title;
    private Integer credit;

    @OneToOne
    private   CourseMaterial courseMaterial;


}
